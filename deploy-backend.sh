#! /bin/bash

function initialize_worker() {
    printf "***************************************************\n\t\tSetting up host \n***************************************************\n"
    # Update packages
    echo ======= Updating packages ========
    sudo apt-get update

    # Export language locale settings
    #echo ======= Exporting language locale settings =======
    #export LC_ALL=C.UTF-8
    #export LANG=C.UTF-8

    # Install pip3
    #echo ======= Installing pip3 =======
    #sudo apt-get install -y python3-wheel
    #sudo apt-get install -y  gcc libpq-dev

    # Install virtualenv
    echo ======= Installing virtualenv =======
    sudo apt-get -y install python3-venv
}


function clone_app_repository_big7() {
    printf "***************************************************\n\t\tFetching App \n***************************************************\n"
    # Clone and access project directory
    echo ======== Cloning and accessing project directory ========
    if [[ -d ~/api_big7 ]]; then
        sudo rm -rf ~/api_big7
        git clone -b master https://gitlab.com/stefanhunziker92/api.big7 ~/api_big7
        cd ~/api_big7/
    else
        git clone -b master https://gitlab.com/stefanhunziker92/api.big7 ~/api_big7
        cd ~/api_big7/
    fi
}


function setup_python_venv_big7() {
    printf "***************************************************\n\t\tSetting up Venv \n***************************************************\n"

    # Create virtual environment and activate it
    echo ======== Creating and activating virtual env =======
    python3 -m venv env_big7
    source ./env_big7/bin/activate

    # install wheel here because in requirements.txt it will fail
    echo ======== Installing wheel =======
    pip3 install wheel

    # install gunicorn via pip, using sudo-apt will lead to errors
    echo ======== Installing wheel =======
    pip3 install gunicorn
}



function install_depencies_big7() {
    printf "***************************************************\n    Installing App dependencies\n***************************************************\n"
    # Install required packages
    echo ======= Installing required packages ========
    pip install -r requirements.txt
}


function add_autodeploy_big7(){
    printf "***************************************************\n    creating File for automated deploy after reboot \n***************************************************\n"
    sudo chmod a+rwx /etc/systemd/system
echo "[Unit]
Description=Gunicorn instance for api_big
After=network.target

[Service]
User=ubuntu
Group=www-data
WorkingDirectory=/home/ubuntu/api_big7
ExecStart=/home/ubuntu/api_big7/env_big7/bin/gunicorn -b 0.0.0.0:8000 app:app
Restart=always

[Install]
WantedBy=multi-user.target" > /etc/systemd/system/api_big7.service

    sudo systemctl daemon-reload
    sudo systemctl start api_big7
    sudo systemctl enable api_big7

    deactivate
}


function clone_app_repository_frivol() {
    printf "***************************************************\n\t\tFetching App \n***************************************************\n"
    # Clone and access project directory
    echo ======== Cloning and accessing project directory ========
    if [[ -d ~/api_frivol ]]; then
        sudo rm -rf ~/api_frivol
        git clone -b master https://gitlab.com/stefanhunziker92/api_frivol ~/api_frivol
        cd ~/api_frivol/
    else
        git clone -b master https://gitlab.com/stefanhunziker92/api_frivol ~/api_frivol
        cd ~/api_frivol/
    fi
}


function setup_python_venv_frivol() {
    printf "***************************************************\n\t\tSetting up Venv \n***************************************************\n"

    # Create virtual environment and activate it
    echo ======== Creating and activating virtual env =======
    python3 -m venv env_frivol
    source ./env_frivol/bin/activate

    # install wheel here because in requirements.txt it will fail
    echo ======== Installing wheel =======
    pip3 install wheel

    # install gunicorn via pip, using sudo-apt will lead to errors
    echo ======== Installing wheel =======
    pip3 install gunicorn
}



function install_depencies_frivol() {
    printf "***************************************************\n    Installing App dependencies\n***************************************************\n"
    # Install required packages
    echo ======= Installing required packages ========
    pip install -r requirements.txt
}

function add_autodeploy_frivol(){
    printf "***************************************************\n    creating File for automated deploy after reboot \n***************************************************\n"
    sudo chmod a+rwx /etc/systemd/system
echo "[Unit]
Description=Gunicorn instance for api_big
After=network.target

[Service]
User=ubuntu
Group=www-data
WorkingDirectory=/home/ubuntu/api_frivol
ExecStart=/home/ubuntu/api_frivol/env_frivol/bin/gunicorn -b 0.0.0.0:8001 app:app
Restart=always

[Install]
WantedBy=multi-user.target" > /etc/systemd/system/api_frivol.service

    sudo systemctl daemon-reload
    sudo systemctl start api_frivol
    sudo systemctl enable api_frivol
}


function restartServices(){
    sudo systemctl restart nginx
    sudo systemctl daemon-reload
    sudo systemctl start api_big7
    sudo systemctl enable api_big7
    sudo systemctl start api_frivol
    sudo systemctl enable api_frivol
}



######################################################################
########################      RUNTIME       ##########################
######################################################################

initialize_worker
clone_app_repository_big7
setup_python_venv_big7
install_depencies_big7
add_autodeploy_big7
clone_app_repository_frivol
setup_python_venv_frivol
install_depencies_frivol
add_autodeploy_frivol
restartServices